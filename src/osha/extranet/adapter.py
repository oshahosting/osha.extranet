from plone.app.users.browser.personalpreferences import UserDataPanelAdapter
from plone.app.layout.globals.layout import LayoutPolicy

from Products.CMFCore.utils import getToolByName
from zope.component import queryUtility
from plone.i18n.normalizer.interfaces import IIDNormalizer
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

try:
    from five.pt.pagetemplate import ViewPageTemplateFile as \
    ViewPageTemplateFileChameleon
    check_against = [ViewPageTemplateFile, ViewPageTemplateFileChameleon]
except ImportError:
    check_against = [ViewPageTemplateFile]


class ExtranetUserDataPanelAdapter(UserDataPanelAdapter):
    """
    Use _getProperty so that all values are decoded from utf-8, all
    fields in the property sheet are stored as unicode.
    """
    def get_mobileTelephoneNumber(self):
        return self._getProperty('mobileTelephoneNumber')
    def set_mobileTelephoneNumber(self, value):
        if value is not None:
            return self.context.setMemberProperties({'mobileTelephoneNumber': value})
    mobileTelephoneNumber = property(get_mobileTelephoneNumber, set_mobileTelephoneNumber)

    def get_shadowExpire(self):
        return self._getProperty('shadowExpire')
    def set_shadowExpire(self, value):
        if value is not None:
            return self.context.setMemberProperties({'shadowExpire': value})
    shadowExpire = property(get_shadowExpire, set_shadowExpire)

    def get_pers_title(self):
        return self._getProperty('pers_title')
    def set_pers_title(self, value):
        if value is not None:
            return self.context.setMemberProperties({'pers_title': value})
    pers_title = property(get_pers_title, set_pers_title)

    def get_facsimileTelephoneNumber(self):
        return self._getProperty('facsimileTelephoneNumber')
    def set_facsimileTelephoneNumber(self, value):
        if value is not None:
            return self.context.setMemberProperties({'facsimileTelephoneNumber': value})
    facsimileTelephoneNumber = property(get_facsimileTelephoneNumber, set_facsimileTelephoneNumber)

    def get_ictBirthday(self):
        return self._getProperty('ictBirthday')
    def set_ictBirthday(self, value):
        if value is not None:
            return self.context.setMemberProperties({'ictBirthday': value})
    ictBirthday = property(get_ictBirthday, set_ictBirthday)

    def get_department(self):
        return self._getProperty('department')
    def set_department(self, value):
        if value is not None:
            return self.context.setMemberProperties({'department': value})
    department= property(get_department, set_department)

    def get_alternativeEmail(self):
        return self._getProperty('alternativeEmail')
    def set_alternativeEmail(self, value):
        if value is not None:
            return self.context.setMemberProperties({'alternativeEmail': value})
    alternativeEmail = property(get_alternativeEmail, set_alternativeEmail)

    def get_homeTelephoneNumber(self):
        return self._getProperty('homeTelephoneNumber')
    def set_homeTelephoneNumber(self, value):
        if value is not None:
            return self.context.setMemberProperties({'homeTelephoneNumber': value})
    homeTelephoneNumber = property(get_homeTelephoneNumber, set_homeTelephoneNumber)

    def get_homePostalAddress(self):
        return self._getProperty('homePostalAddress')
    def set_homePostalAddress(self, value):
        if value is not None:
            return self.context.setMemberProperties({'homePostalAddress': value})
    homePostalAddress = property(get_homePostalAddress, set_homePostalAddress)

    def get_ictTimeZone(self):
        return self._getProperty('ictTimeZone')
    def set_ictTimeZone(self, value):
        if value is not None:
            return self.context.setMemberProperties({'ictTimeZone': value})
    ictTimeZone = property(get_ictTimeZone, set_ictTimeZone)

    def get_pager(self):
        return self._getProperty('pager')
    def set_pager(self, value):
        if value is not None:
            return self.context.setMemberProperties({'pager': value})
    pager = property(get_pager, set_pager)

    def get_ictHomeFacsimileTelephoneNumber(self):
        return self._getProperty('ictHomeFacsimileTelephoneNumber')
    def set_ictHomeFacsimileTelephoneNumber(self, value):
        if value is not None:
            return self.context.setMemberProperties({'ictHomeFacsimileTelephoneNumber': value})
    ictHomeFacsimileTelephoneNumber = property(get_ictHomeFacsimileTelephoneNumber, set_ictHomeFacsimileTelephoneNumber)

    def get_telephoneNumber(self):
        return self._getProperty('telephoneNumber')
    def set_telephoneNumber(self, value):
        if value is not None:
            return self.context.setMemberProperties({'telephoneNumber': value})
    telephoneNumber = property(get_telephoneNumber, set_telephoneNumber)

    def get_givenName(self):
        return self._getProperty('givenName')
    def set_givenName(self, value):
        if value is not None:
            return self.context.setMemberProperties({'givenName': value})
    givenName = property(get_givenName, set_givenName)

    def get_ictGender(self):
        return self._getProperty('ictGender')
    def set_ictGender(self, value):
        if value is not None:
            return self.context.setMemberProperties({'ictGender': value})
    ictGender = property(get_ictGender, set_ictGender)

    def get_sn(self):
        return self._getProperty('sn')
    def set_sn(self, value):
        if value is not None:
            return self.context.setMemberProperties({'sn': value})
    sn = property(get_sn, set_sn)

    def get_ictFavoriteLinks(self):
        return self._getProperty('ictFavoriteLinks')
    def set_ictFavoriteLinks(self, value):
        return self.context.setMemberProperties({'ictFavoriteLinks': value})
    ictFavoriteLinks = property(get_ictFavoriteLinks, set_ictFavoriteLinks)

    def get_oshPersSubjects(self):
        return self._getProperty('oshPersSubjects')
    def set_oshPersSubjects(self, value):
        return self.context.setMemberProperties({'oshPersSubjects': value})
    oshPersSubjects = property(get_oshPersSubjects, set_oshPersSubjects)

    def get_initials(self):
        return self._getProperty('initials')
    def set_initials(self, value):
        if value is not None:
            return self.context.setMemberProperties({'initials': value})
    initials = property(get_initials, set_initials)


class OSHALayoutPolicy(LayoutPolicy):
    """A view that gives access to various layout related functions.
    """
    
    def bodyClass(self, template, view):
        """Returns the CSS class to be used on the body tag.
        """
        context = self.context
        url = getToolByName(context, "portal_url")

        # template class (required)
        name = ''
        browser_view = False
        for klass in check_against:
            if isinstance(template, klass):
                browser_view = True
        if browser_view:
            # Browser view
            name = view.__name__
        else:
            name = template.getId()
        body_class = 'template-%s' % name

        # portal type class (optional)
        normalizer = queryUtility(IIDNormalizer)
        portal_type = normalizer.normalize(context.portal_type)
        if portal_type:
            body_class += " portaltype-%s" % portal_type

        # section class (optional)
        contentPath = url.getRelativeContentPath(context)
        if contentPath:
            body_class += " section-%s" % contentPath[0]

        return body_class
