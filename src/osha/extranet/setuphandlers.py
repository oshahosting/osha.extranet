import logging
import transaction

from zope import component
from zope import schema

from Products.CMFCore.utils import getToolByName

from plone.app.controlpanel.skins import ISkinsSchema
from plone.registry.interfaces import IRegistry

from collective.xdv.interfaces import ITransformSettings
from config import PRODUCT_DEPENDENCIES
from config import FOLDERISH_TYPES

from Products.PortalTransforms.unsafe_transforms import build_transforms

# ldap / user management
from Products.PloneLDAP.factory import genericPluginCreation
from Products.PloneLDAP.plugins.ldap import PloneLDAPMultiPlugin
from plone.app.controlpanel.usergroups import IUserGroupsSettingsSchema
from plone.app.users.userdataschema import IUserDataSchemaProvider
from plone.app.users.registrationschema import IRegistrationSchema


log = logging.getLogger('osha.extranet/setuphandlers.py')


def installDependencies(context):
    if context.readDataFile("osha-extranet.txt") is None:
        return

    site = context.getSite()
    qi = getToolByName(site, 'portal_quickinstaller')
    for product in PRODUCT_DEPENDENCIES:
        if not qi.isProductInstalled(product):
            log.info("Installing dependency: %s" % product)
            qi.installProduct(product)
            transaction.savepoint(optimistic=True)

def setFolderView(context):
    """ Set 'folder_contents' as the default view for folderish objects
    """
    if context.readDataFile("osha-extranet.txt") is None:
        return
    
    site = context.getSite()
    portal_types = getToolByName(site, 'portal_types')
    content_types = portal_types.listContentTypes()
    for type in FOLDERISH_TYPES:
        info = portal_types.getTypeInfo(type)
        info._updateProperty("default_view", "folder_listing")
        info._updateProperty("immediate_view", "folder_listing")
        aliases = info.getMethodAliases()
        aliases['(Default)'] = 'folder_contents'
        aliases['view'] = 'folder_contents'
        info.setMethodAliases(aliases)


def configureChatService(context):
    """ Configure the chat_service to point to the correct URL"""
    if context.readDataFile("osha-extranet.txt") is None:
        return

    site = context.getSite()
    portal_chat = getToolByName(site, 'portal_chat')
    portal_chat._updateProperty('name', 'chatservice')
    portal_chat._updateProperty('host', 'localhost')
    portal_chat._updateProperty('port', '8011')
    portal_chat._updateProperty('username', 'admin')
    portal_chat._updateProperty('password', 'admin')


def setPloneClassicTheme(context):
    """ """
    if context.readDataFile("osha-extranet.txt") is None:
        return

    site = context.getSite()
    settings = component.getAdapter(site, ISkinsSchema)
    settings.set_theme('Plone Classic Theme')


def configureNavigation(context):
    """ Change NavSettings to generate tabs only for folders and to display
        only folderish types.
    """
    if context.readDataFile("osha-extranet.txt") is None:
        return

    site = context.getSite()
    pprop = getToolByName(site, 'portal_properties')
    pprop.site_properties._updateProperty('disable_nonfolderish_sections', True)
    pprop.navtree_properties._updateProperty('sortAttribute', 'sortable_title')

    ttool = getToolByName(site, 'portal_types')
    all_types = ttool.listContentTypes()
    putils = getToolByName(site, 'plone_utils')
    friendlyTypes = putils.getUserFriendlyTypes()
    
    blacklisted = [t for t in all_types if t not in FOLDERISH_TYPES or t not in friendlyTypes]
    pprop.navtree_properties._updateProperty(
                                'metaTypesNotToList', 
                                blacklisted)

def createDiscussionBoard(context):
    """ Create the global PloneBoard forum
    """
    if context.readDataFile("osha-extranet.txt") is None:
        return

    site = context.getSite()
    if "forum" not in site.objectIds():
        site.invokeFactory('Ploneboard', 'forum', title='Discussion forum')


def setupXDVTheme(context):
    """ Activate and configure collective.xdv
    """
    if context.readDataFile("osha-extranet.txt") is None:
        return

    site = context.getSite()
    settings = component.getUtility(IRegistry).forInterface(ITransformSettings)
    settings.enabled = True
    settings.domains = set([u'localhost:8010', u'extranet.osha.syslab.com'])
    settings.theme = u'python://ictextranet.theme/skins/ictextranet_theme/theme.html'
    settings.rules = u'python://ictextranet.theme/skins/ictextranet_theme/rules/default.xml'
    settings.boilerplate = u'parts/omelette/ictextranet/theme/skins/ictextranet_theme/theme.xsl'
    settings.absolute_prefix = unicode(site.getId())
    default_notheme = [
        u'^.*/emptypage$',
        u'^.*/manage$',
        u'^.*popup$',
        u'^.*/manage_(?!translations_form)[^/]+$',
        u'^.*/image_view_fullscreen$',
        u'^.*/refbrowser_popup(\?.*)?$',
        u'^.*/error_log(/.*)?$',
        u'^.*/aq_parent(/.*)?$',
        u'^.*/portal_javascripts/.*/jscripts/tiny_mce/.*$',
        u'^.*/tinymce-upload$',
        u'^.*/.+/plone(image|link)\.htm$',
        u'^.*/plugins/table/(table|row|cell|merge_cells)\.htm$',
        u'^.*/plugins/searchreplace/searchreplace.htm$',
        u'^.*/.+/advanced/(source_editor|anchor)\.htm$',
        u'^.*/@@babblechat.*$',       # Don't interfere with Babble
        u'^.*/@@render_chat_box',     # Don't interfere with Babble
        u'^.*/manage_addProduct/.*$', # Necesary for ZMI access.
        ]

    if settings.notheme != None:
        settings.notheme = set(default_notheme)
    else:
        for i in default_notheme:
            # Add / re-add any which are missing from the default
            # configuration, this won't remove additional entries
            # which may have been added manually
            settings.notheme = settings.notheme.add(i)


def addUnsafeTransforms(context):
    """ Add transforms that are considered unsafe to portal_transforms
    """
    if context.readDataFile("osha-extranet.txt") is None:
        return

    portal_transforms = getToolByName(context, 'portal_transforms', None)
    if portal_transforms:
        log.info('calling build_transforms.initialize()')
        build_transforms.initialize(portal_transforms)
    else:
        log.warn('no portal_transforms found, not adding unsafe transfers')


def configureLDAP(context):
    """Configure LDAP connection, schema and user registration schema

    XXX: should be split up into several steps
    """
    if context.readDataFile("osha-extranet.txt") is None:
        return
    site = context.getSite()
    pas = site.acl_users
    try:
        pas.ldap
    except AttributeError:
        pass
    else:
        log.info('LDAP Multiplugin already exists. Remove to reinstall.')
        return

    try:
        getattr(pas, 'ldap-plugin')
    except AttributeError:
        pass
    else:
        log.warn('Old LDAP Multiplugin already exists. Uninstall '
                'plone.app.ldap and remove ldap-plugin from acl_users.')
        return

    # add multiplugin, yes it returns the contained LDAPUserFolder
    luf=genericPluginCreation(pas, PloneLDAPMultiPlugin,
            id="ldap",
            title="LDAP",
            login_attr="uid",
            uid_attr="uid",
            rdn_attr='uid',
            users_base="ou=people,o=extranet,dc=osha,dc=europa,dc=eu",
            users_scope=2,
            obj_classes="inetOrgPerson,shadowAccount,ictPerson",
            roles="Member",
            groups_base="",
            groups_scope=2,
            #LDAP_server="127.0.0.1:3456",
            binduid="cn=manager,o=extranet,dc=osha,dc=europa,dc=eu",
            bindpwd="secret",
            binduid_usage=1,
            local_groups=1,
            use_ssl=0,
            encryption='SHA',
            read_only=0,
            )
    log.info('Added LDAP Multiplugin.')
    log.warn('You need to configure ldap server, binduid and bindpwd in '
            'ZMI/acl_users/ldap/acl_users.')

    # these two exist already
    luf._ldapschema["cn"]["public_name"]="fullname"
    luf._ldapschema["sn"]["public_name"]="sn"
    luf._ldapschema["sn"]["friendly_name"]="Surname"

    # these are new
    for ldap_name, public_name, friendly_name in (
        ("mail", "email", "Email Address"),
        ("ictAlternativeMail", "alternativeEmail", "Alternative E-mail"),
        ("departmentNumber", "department", "Department"),
        ("facsimileTelephoneNumber", "facsimileTelephoneNumber", "Fax"),
        ("homePostalAddress", "homePostalAddress", "Home Address"),
        ("homePhone", "homeTelephoneNumber", "Home Phone"),
        ("ictBirthday", "ictBirthday", "Birthday"),
        ("ictFavoriteLinks", "ictFavoriteLinks", "My Favorite Links"),
        ("ictGender", "ictGender", "Gender"),
        ("ictHomeFacsimileTelephoneNumber", "ictHomeFacsimileTelephoneNumber",
                "Home Fax"),
        ("ictMyHomepage", "home_page", "Homepage"),
        ("ictTimeZone", "ictTimeZone", "Time Zone"),
        ("initials", "initials", "Middle Initial"),
        ("l", "location", "Office Location"),
        ("mobile", "mobileTelephoneNumber", "Cellular Phone"),
        ("oshPersSubjects", "oshPersSubjects", "Interests"),
        ("pager", "pager", "Pager"),
        ("telephoneNumber", "telephoneNumber", "Phone"),
        ("title", "pers_title", "Title"),
        ("givenName", "givenName", "First Name"),
        ):
        luf.manage_addLDAPSchemaItem(
                ldap_name=ldap_name,
                public_name=public_name,
                friendly_name=friendly_name,
                )
    luf._ldapschema["ictFavoriteLinks"]["multivalued"]=True
    luf._ldapschema["oshPersSubjects"]["multivalued"]=True
    log.info('Configured ldap schema')

    # activate plugin
    iface_names = [
            'IUserEnumerationPlugin',
#            'IGroupsPlugin',
#            'IGroupEnumerationPlugin',
            'IRoleEnumerationPlugin',
            'IAuthenticationPlugin',
            'ICredentialsResetPlugin',
            'IPropertiesPlugin',
            'IRolesPlugin',
#            'IGroupIntrospection',
#            'IGroupManagement',
            'IUserAdderPlugin',
            'IUserManagement',
            ]

    pas.ldap.manage_activateInterfaces(iface_names)
    plugins=pas.plugins

    for iface_name in iface_names:
        iface = plugins._getInterfaceFromName(iface_name)
        for i in range(len(plugins.listPlugins(iface)) - 1):
            plugins.movePluginsUp(iface, ['ldap'])
    log.info('Activated LDAP and moved first-place for: %s' % (iface_names,))

    IUserGroupsSettingsSchema(site).many_users = True
    log.info('Configured Plone users view for many users')

    # enable RAMCache (which we replaced by Memcached
    # XXX: PAS RAMCache leads to user property changes through plone not being
    # visible, there is some cache invalidate missing. However, we have at
    # least caching on the ldapmultiplugin
    #if pas.ZCacheable_getManager() is None:
    #    pas.ZCacheable_setManagerId(manager_id="RAMCache")
    #    log.info('Set RAMCache as cache manager for PAS')
    pas.ldap.ZCacheable_setManagerId(manager_id="RAMCache")
    log.info('Set RAMCache as cache manager for ldap multiplugin')

    # enable our fields in the user registration form
    skip_fields = ('pdelete', 'portrait')
    user_schema = component.getUtility(IUserDataSchemaProvider).getSchema()
    # this is not a schema in the sense of zope.schema
    reg_schema = IRegistrationSchema(site)
    our_reg_schema = tuple(
            [x for x in schema.getFieldNamesInOrder(user_schema) \
                    if not x in skip_fields]
            )
    if not reg_schema.user_registration_fields == our_reg_schema:
        log.info('Registration schema before: %s' % \
                (reg_schema.user_registration_fields,))
        reg_schema.user_registration_fields = our_reg_schema
        log.info('Registration schema now: %s' % \
                (reg_schema.user_registration_fields,))
    else:
        log.info('Registration schema already is ours.')
