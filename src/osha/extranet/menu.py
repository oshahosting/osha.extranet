from plone.memoize.instance import memoize
from plone.app.contentmenu.menu import ActionsSubMenuItem 
from Products.CMFCore.utils import getToolByName

class OSHAActionsSubMenuItem(ActionsSubMenuItem):

    # @memoize
    def available(self):
        """ We want the 'actions' submenu to appear on 'folder_contents', so we
            overwrite this method.
        """
        # if IContentsPage.providedBy(self.request):
        #     return False
        actions_tool = getToolByName(self.context, 'portal_actions')

        editActions = actions_tool.listActionInfos(
                                        object=self.context, 
                                        categories=('object_buttons',), 
                                        max=1
                                        )
        return len(editActions) > 0


