# Here is a list of products that don't have genericsetup profiles and need to
# be installed manually. See setuphandlers.py
PRODUCT_DEPENDENCIES = [
    'Scrawl',
    'Products.MaildropHost',
    'Products.SecureMaildropHost',
    'p4a.plonecalendar',
    'slc.statisticsannotation'
    ]

FOLDERISH_TYPES = [
    'Plone Site',
    'Folder',
    'Blog',
#    'Ploneboard',
#    'PloneboardForum',
#    'HelpCenter',
#    'HelpCenterFAQFolder',
    'Gallery',
    'Topic'
    ]

GLOBALS = globals()
