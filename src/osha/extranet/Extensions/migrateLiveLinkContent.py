# -*- coding: utf-8 -*-

import logging
import os
import pickle
import re
import transaction

from DateTime import DateTime
from persistent.dict import PersistentDict

from zope.annotation.interfaces import IAnnotations
from zope.site.hooks import getSite

from Products.Archetypes.utils import shasattr
from Products.CMFCore.WorkflowCore import WorkflowException
from Products.CMFCore.utils import getToolByName

from BeautifulSoup import BeautifulStoneSoup

task_status_mapping = {
    'Pending': '',
    'In Process': 'start_work_on',
    'Issue': '',
    'On Hold': 'put_on_hold',
    'Completed': 'mark_as_complete',
    'Cancelled': '',
    }

role_mapping = {
    u'See': 'Reader',
    u'SeeContent': 'Reader',
    u'Modify': 'Editor',
    u'EditAttr': 'Editor',
    u'Create': 'Contributor',
    u'DeleteVersion': 'Reviewer',
    u'Delete': 'Editor',
    u'Reserve': 'Reviewer',
    u'EditPerm': 'Editor',
    u'Write': 'Editor',
    u'Read': 'Reader',
    u'Administer': 'Editor',
    }

type_mapping = {
    'URL': 'Link',
    'Live Report': 'Folder',
    'Milestone': 'Folder',
    'TaskList': 'Folder',
    'Task Group': 'Folder',
    'Task': 'Task',
    'Discussion': 'Ploneboard',
    'Folder': 'Folder',
    'Document': 'File',
    'Project': 'Folder',
    'Image': 'Image',
    'Meeting Room': 'Folder',
    'CompoundDocument': 'Folder',
    'Poll': 'PlonePopoll',
    'Workflow Map': 'Folder',
    'Workflow Attachment': 'Folder',
    'Discussion Entry': 'PloneboardComment',
    }

field_mapping = {
    'Name': 'title',
    'Description': 'description',
    'Instructions': 'description',
    'Created': 'creation_date',
    'Creation Date': 'creation_date',
    'Modified': 'modification_date',
    'Due Date': 'dueDate',
    'Start Date': 'startDate',
    'Completion Date': 'completionDate',
    'Priority': 'priority',
    'Comments': 'comments',
    'Assigned To': 'assignedTo',
    'Agency contact person': 'agencyContact',
    'URL': 'remoteUrl',
    'file_contents': 'file',
    'classifications': 'subject',
}

log = logging.getLogger('osha.extranet/Extensions.migrateLiveLinkContent.py')

# ====================== MIGRATION FROM LIVELINK  ===============================

# Variables for tweaking the migration:
# ---------------------------------------------------------------
dry_run = False             # During dry run, nothing is set except for 
                            # objects of type in overwrite_types or with 
                            # id in overwrite_objects

overwrite_types = []  # List of types of objs to write
                            # Takes precedence over 'dry_run'

overwrite_objects = []      # List of ids of objs to write
                            # Takes precedence over 'dry_run'

regenerate_tree = False
regenerate_users_map = False
regenerate_already_handled = True

recursion_depth = -1

data_dir = '/home/extractor/extranet_final/'
users_dir = '/home/extractor/users_final/'
# ---------------------------------------------------------------

members_not_found = []
groups_not_found = []
user_ids_not_found = []

def run(self):
    """ This is the main migration method that is called from portal_setup.
    """
    log.info('migrateContent called')
    log.info('regenerate_tree: %s' % str(regenerate_tree))
    log.info('data_dir: %s' % str(data_dir))

    site = getSite()

    if regenerate_users_map:
        user_mapping = getUserMapping()
        site._setOb('user_mapping', user_mapping)
        log.info('Finished reading the user migration files')
        transaction.commit()

    if regenerate_tree:
        tree = PersistentDict()
        tree['ROOT'] = {}
        tree = createObjectIDTree(data_dir, tree)
        site._setOb('migration_data', tree)
        log.info('Finished reading the data migration files')
        transaction.commit()

    if regenerate_already_handled:
        handled = list()
        site._setOb('already_handled', handled)
        log.info('Reset the already_handled list')
        transaction.commit()


    user_mapping = site._getOb('user_mapping')
    tree = site._getOb('migration_data')
    already_handled = site._getOb('already_handled')
    log.info('Already handled are: %d items (without children)' %len(already_handled))

    log.info('Starting migrating content...')

    # According to do3cc '2000' is the real root.
    recurse(site, tree['ROOT']['2000'], user_mapping, 0, already_handled)

    if members_not_found:
        log.error("The following Members weren't found: ")
        log.error('\n'.join([mid for mid in members_not_found]))
        log.error('-------------------------------------')

    if groups_not_found:
        log.error("The following Groups weren't found: ")
        log.error('\n'.join([gid for gid in groups_not_found]))
        log.error('-------------------------------------')

    if user_ids_not_found:
        log.error("Users Ids could not be found for the following names: ")
        log.error('\n'.join([uid for uid in user_ids_not_found]))
        log.error('-------------------------------------')

    log.info('Migration finished')

    return "Finished, all is well"


def getUserMapping():
    """ Get the mapping/dict of User fullnames with user ids
    """
    deamp = re.compile('&#(x?)([a-zA-Z0-9]{1,3});')
    def unescape(str):
        if not str:
            return str

        str.strip()

        ret = str
        match = deamp.search(ret)
        while match:
            if match.group(1):
                base = 16
            else:
                base = 10
            ret = deamp.sub(unicode(unichr(int(match.group(2), base))), ret)
            match = deamp.search(ret)
        ret = ret.replace(u'&scaron;', unicode(unichr(353)))
        ret = ret.replace(u'&Scaron;', unicode(unichr(352)))
        ret = ret.replace(u'&ndash;', unicode(unichr(8211)))
        return ret

    users = list()
    groups = list()


    for subdir, dirs, files in os.walk(users_dir):
        log.info('Reading users files in %s' % str(subdir))
        for file in files:
            try:
                f=open(users_dir+file, 'r')
                data = pickle.load(f)
            except KeyError, e:
                log.error(e)
            except IOError, e:
                log.error(e)
            f.close()

            if 'group_name' in data:
                groups.append(data)

            elif 'Log-in Name' in data:
                users.append(data)

    user_mapping = PersistentDict()
    for u in users:
        username = unescape(u[u'Log-in Name'])
        for old, new in {u'ä' : 'a', u'á' : 'a', u'ü' : 'u', u'ö' : 'o', u'ø' : 'o'}.items():
            username = username.replace(old, new)

        if u.has_key(u'First Name') and u.has_key(u'Last Name'):
            fullname =  u'%s %s' % (u[u'First Name'], u[u'Last Name'])
            user_mapping[fullname] = username.strip()
        else:
            user_mapping[u'Log-in Name'] = username

    return user_mapping


def createObjectIDTree(data_dir, tree={}):
    """ Create a tree (dicts of dicts of dicts) of the IDs of the migrated pickles 
    """
    for subdir, dirs, files in os.walk(data_dir):
        log.info('Reading %d data migration files in %s' % (len(files), str(subdir)))
        i = 0
        for file in files:
            try:
                f=open(data_dir+file, 'r')
                fdict = pickle.load(f)
                f.close()
            except KeyError, e:
                log.error(e)
                continue
            except IOError, e:
                log.error(e)
                continue
            except IndexError, e:
                log.error(e)
                continue

            i += 1
            if i%1000 == 0:
                log.info('%s files read' % str(i))

            if not fdict.has_key('parents'):
                log.info(
                    'Item without parents found: %s, %s' \
                    % (fdict.get('content_type', 'Unknown'), fdict['id']))
                continue

            parents = fdict['parents'][0] # what's going on here...

            # XXX: Workaround. There is a bug in the migrated data, 
            # that causes some parents to be repeated.
            fixed_parents = []
            for p in parents:
                if p not in fixed_parents:
                    fixed_parents.append(p)
            parents = fixed_parents

            subtree = tree
            for parent in parents:
                node = subtree.get(parent)
                if node is None:
                    subtree[parent] = {}
                    subtree = subtree[parent]
                else:
                    subtree = node
    return tree


def recurse(context, tdict, user_mapping, level, already_handled):
    for key in tdict.keys():
        try:
            f=open(data_dir + key, 'r')
        except IOError:
            log.error('Could not open file %s' % str(data_dir+key))
            return
            
        fdict = pickle.load(f)
        if not fdict.has_key('content_type'):
            log.info('Skipping %s, it is missing a content_type' % fdict['id'])
            continue

        content_type = fdict['content_type']    
        if fdict.has_key('MIME Type') and 'image' in fdict['MIME Type']:
            content_type = 'Image'
        elif fdict.has_key('MIME Type') and 'html' in fdict['MIME Type']:
            content_type = 'Document'
        else:
            content_type = type_mapping[content_type]

        log.info('Recurse: %s' % fdict['id'])
        # hack, we skip PloneboardComment
        if not dry_run or fdict['id'] in overwrite_objects or content_type in overwrite_types \
            and content_type!="PloneboardComment":
            if fdict['id'] in already_handled:
                obj = context._getOb(fdict['id'])
                log.info('Skip, obj was already handled')
            else:
                obj = migrateObject(context, fdict, content_type, user_mapping)
                transaction.commit()
                if len(tdict[key])==0:
                    already_handled.append(fdict['id'])
                    site = getSite()
                    site._setOb('already_handled', already_handled)
                    log.info('Added %s to list of already handled' %fdict['id'])
                    transaction.commit()
        else:
            obj = context._getOb(fdict['id'])

        level += 1
        if level < recursion_depth or recursion_depth == -1 and obj is not None:
            recurse(obj, tdict[key], user_mapping, level, already_handled)
        

def createObject(context, fdict, content_type):
    if content_type == 'PloneboardComment':
        # We don't handle comments afaik...? (thomasw)
        return None

        # In LiveLink comments are created in comments. In Ploneboard, they are flat.
        while context.meta_type == 'PloneboardComment':
            context = context.aq_parent

        if context.meta_type == 'Ploneboard':
            # The topmost 'Discussion Entry' is not a PloneboardComment, 
            # but a PloneboardConversation
            content_type = 'PloneboardConversation'

        elif context.meta_type != 'PloneBoardConversation':
            # The contex is not a Ploneboard, Conversation or Comment. So we need to first 
            # create a Ploneboard here, and then a PloneboardConversation out of this current 
            # 'Discussion Entry'.
            import pdb; pdb.set_trace()
            context = createObject(context, fdict, 'Ploneboard')
            content_type = 'PloneboardConversation'

        else:
            # The content_type is 'PloneBoardConversation', which is perfect.
            pass 

    pt = getToolByName(context, 'portal_types')
    if not shasattr(context, fdict['id']):
        info = pt.getTypeInfo(content_type)
        try:
            obj = info._constructInstance(context, fdict['id'])
            obj.reindexObject()
            log.info('Created: %s, %s' % (content_type, fdict['id']))
        except Exception as e:
            log.error('Error when trying to created object '
                'with id %s' % fdict['id'])
            log.error(e)

    log.info('Handling  %s, %s' % (content_type, fdict['id']))
    return context._getOb(fdict['id'])


def migrateObject(context, fdict, content_type, user_mapping):
    """ """
    obj = createObject(context, fdict, content_type)
    if obj is None:
        return obj
    obj = setDisplayProperties(context, fdict, obj)
    obj = setObjectFields(context, fdict, obj, user_mapping, content_type)
    obj = postCreationHandling(context, fdict, obj, content_type)

    return obj


def setDisplayProperties(context, fdict, obj):
    """
    Get the Annotations on the parent.
    There should be a 'livelink_content' entry, which contains the display 
     properties for this parent's children.
    
    The display properties are 'Link', 'Catalog' or 'hidden'
    
    We see if this object is mentioned in the parent's display properties and 
    then set its excludeFromNav flag or workflow state accordingly.
    """
    parent = obj.aq_parent
    wt = getToolByName(context, "portal_workflow")
    annotations = IAnnotations(parent)
    if annotations.has_key('livelink_content'):
        llcontent = annotations.get('livelink_content')
        display_attr = [x['display'] for x in llcontent if x['id'] == fdict['id']]

        if display_attr: 

            exclude_from_nav = False
            if display_attr[0]  == 'List':
                exclude_from_nav = True

            elif display_attr[0] == 'Hidden':
                chain = wt.getChainFor(context)
                status = wt.getStatusOf(chain[0], object)
                if status != 'hidden':
                    try:
                        wt.doActionFor(obj, "hide")
                    except WorkflowException:
                        log.error('Could not set the workflow state to hidden for %s' % obj.absolute_url())

            obj.getField('excludeFromNav').set(obj, exclude_from_nav)
    return obj


def setObjectFields(context, fdict, obj, user_mapping, content_type):
    """ """
    acl_users = context.acl_users
    pg = getToolByName(context, 'portal_groups')
    wt = getToolByName(context, "portal_workflow")
    schema = obj.Schema()

    for k in fdict.keys():

        if k == u'Owned By':
            fullname = ' '.join([s.strip() for s in fdict[k].split(' ')]).strip()
            member_id = user_mapping.get(fullname)

            if not member_id:
                if fullname not in user_ids_not_found:
                    user_ids_not_found.append(fullname)
                    log.error('Could not get userid from mapping for %s' % fullname)
                continue

            try:
                user = acl_users.getUserById(member_id) 
            except Exception as e:
                log.error(u"Error when calling getUserById"
                    "with id %s, the key is %s, the id is %s" % (member_id, k, fdict['id']))
                log.error(e)
                continue

            if not user:
                if member_id not in members_not_found:
                    log.error("Could not get user for id '%s'" % member_id)
                    members_not_found.append(member_id)
                continue 

            obj.changeOwnership(user, recursive=False)
            obj.reindexObjectSecurity()
            continue

        if k == u'Created By':
            creators = [fdict[k]]
            schema['creators'].set(obj, creators)
            continue

        if k == 'group_owner':
            # This is a group that has ownership of this object, but with its
            # permission declared explicitly, not as in the Plone sense where
            # permissions for ownership are implicit.
            #
            # {u'perms': [u'See', u'SeeContent'], u'user_type': u'group', u'name': u'Agency'}

            mapping = fdict[k]
            perms = mapping[u'perms']
            if not perms:
                continue 

            name = mapping[u'name']
            group = pg.getGroupById(name)
            if group is None:
                if name not in groups_not_found:
                    log.error('Could not get group by id %s' % name)
                    groups_not_found.append(name)
                continue 

            roles = [role_mapping[i] for i in perms if i not in [None, u'None']]
            if not roles:
                continue

            unique_roles = []
            for r in roles:
                if r not in unique_roles:
                    unique_roles.append(r)
                    
            obj.manage_setLocalRoles(group.getId(), unique_roles)
            continue

        if k == u'public':
            # This is kinda like Plone's 'Authenticated', i.e what 
            # roles/permissions do the public/logged-in-users have here
            #
            # {u'perms': [], u'user_type': u'public', u'name': u'Public Access'}
            mapping = fdict[k]
            perms = mapping[u'perms']
            if not perms:
                continue 

            roles = [role_mapping[i] for i in perms if i not in [None, u'None']]
            if not roles:
                continue 

            unique_roles = []
            for r in roles:
                if r not in unique_roles:
                    unique_roles.append(r)
                    
            obj.manage_setLocalRoles('AuthenticatedUsers', unique_roles)
            continue

        if k == u'others':
            # This contains the local_roles mapping for the object 
            # (aside from 'Owner', 'group_owner' and 'public')
             
            roles_and_perms_list = fdict[k]
            for mapping in roles_and_perms_list:
                name = mapping[u'name']
                type = mapping[u'user_type']
                perms = mapping[u'perms']
                if not perms:
                    continue 

                if type == u'group':
                    user_or_group = pg.getGroupById(name)
                    if user_or_group is None:
                        if name not in groups_not_found:
                            log.error('Could not get group by id %s' % name)
                            groups_not_found.append(name)
                        continue
                else:
                    member_id = user_mapping.get(name.strip())
                    if not member_id:
                        if name.strip() not in user_ids_not_found:
                            log.error('Could not get userid from mapping for %s' % name)
                            user_ids_not_found.append(name.strip())
                        continue

                    try:
                        user_or_group = acl_users.getUserById(member_id)
                    except Exception as e:
                        log.error(u"Error when calling getUserById"
                            "with id '%s', the key is '%s', the id is '%s'" % (member_id, k, fdict['id']))
                        log.error(e)
                        continue

                    if not user_or_group:
                        if member_id not in members_not_found:
                            log.error("Could not get member for id '%s'" % member_id)
                            members_not_found.append(member_id)
                        continue 

                roles = [role_mapping[i] for i in perms if i not in [None, u'None']]
                if not roles:
                    continue 

                unique_roles = []
                for r in roles:
                    if r not in unique_roles:
                        unique_roles.append(r)
                        
                obj.manage_setLocalRoles(user_or_group.getId(), unique_roles)

            continue

        if k == u'content':
            # u'Content' contains the display properties for this item's
            # children.
            #
            # Set the display properties as an annotation on this item.
            annotations = IAnnotations(obj)
            annotations['livelink_content'] = fdict[k]
            continue

        if k == u'Status' and content_type == 'Task':
            chain = wt.getChainFor(obj)
            status = wt.getStatusOf(chain[0], object)
            action = task_status_mapping[fdict[k]]
            if status != 'hidden' and action != '':
                try:
                    wt.doActionFor(obj, action)
                except WorkflowException as e:
                    log.error("Could not set the workflow state to '%s'"
                              "for %s of type %s" % (action, obj.getId(), content_type))
                    log.error(e)
                except Exception as e:
                    log.error("Could not set the workflow state to '%s' due to an UNKNOWN ERROR"
                        "for %s of type %s" % (action, obj.getId(), content_type))
                    log.error(e)

            continue

        if k == u'Display':
            # XXX: According to do3cc, this cannot be completely trusted for
            # completeness. 
            continue 

            wt = getToolByName(context, "portal_workflow")
            if fdict[k] == 'List':
                exclude_from_nav = True
            elif fdict[k] == 'hidden':
                try:
                    wt.doActionFor(obj, "hide")
                except WorkflowException:
                    log.error('Could not set the workflow state to hidden for %s' % obj.absolute_url())
            else:
                exclude_from_nav = False

            try:
                obj.getField('excludeFromNav').set(obj, exclude_from_nav)
            except AttributeError:
                log.error("Object %s doesn't have excludeFromNav" % obj.meta_type)
            continue

        if not field_mapping.has_key(k):
            # XXX: keys that we don't use (yet?)
            # -----------------------------------------------------------
            # owner: This is the owner role information. We already have
            #       u'Owned By' with which we set the owner of the object, 
            #       so this is probably redundant.
            #
            # Size: In plone the size is never set anywhere
            #
            # members: ?????????
            #
            # News Player: ??????????
            #
            # # of Sub-Catalog Items: ??????????
            #
            # Original Item: ?????????????
            # 
            # 1st Banne: ???????????????
            #
            # 2nd Banne: ???????????????
            #
            # Type: seems to be a duplicate of content_type
            #
            # Milestone: reference to milestone (not implemented)
            # 
            # Default Milestone for Tasks: Milestones not being implemented
            #
            # [MIME Type, content_type, id, parents]: the data for these 
            #       is already processed earlier
            # 
            #
            if k not in ['id', 'parents', 
                        'content_type', 
                        'Size', 'Type', 
                        'owner', 'MIME Type',
                        'members', 'News Player',
                        '# of Sub-Catalog Items', 
                        'Original Item', 'Milestone',
                        ]:

                log.info('For type: %s, field_mapping does not have key: \
                        %s' % (content_type, k))
            continue

        if k == u'file_contents' and content_type == 'Image':
            field_name = 'image'
        elif k == u'file_contents' and content_type == 'Document':
            field_name = 'text'
        else:
            field_name = field_mapping[k]

        field = schema.get(field_name)

        if field:
            if field.type == 'datetime':   
                value = DateTime(fdict[k])

            elif field.__name__ == 'text':
                value = fdict[k]
                soup = BeautifulStoneSoup(value)
                for body in soup.findAll('body'):
                    value = str(body)
                    break
            else:
                value = fdict[k]

            if value != field.get(obj):
                field.set(obj, value)

        else:
            log.error('for obj: %s, field %s does not exist' \
                                    % (obj.getId(), field_name))

    return obj

def postCreationHandling(context, fdict, obj, content_type):
    """ Additional steps performed after the object is
    created and properly initialised."""
    if content_type == 'File':
        title = obj.Title()
        obj.setFilename(title)
        log.info("Set filename on %s to '%s'" %(obj.absolute_url(), title))

        if fdict.has_key('MIME Type'):
            obj.getFile().setContentType(fdict['MIME Type'])
            log.info("Set content type on %s to '%s'" %(obj.absolute_url(), fdict['MIME Type']))
        log.info("Set filename on %s to '%s'" %(obj.absolute_url(), title))
    elif content_type == 'Document':
        # special hack for html text in customview.html
        if obj.Title() == 'customview.html':
            if getattr(obj, 'content_type', '') == 'text/plain':
                field = obj.getField('text')
                field.setContentType(obj, 'text/html')

    return obj
