import cPickle
from Acquisition import aq_inner, aq_base

IDX = ['SearchableText', 'Title', 'Description']
dumppath = '/tmp'

def dump(self):
  pc = self.portal_catalog
  msg = ""
  for idx in IDX:
    idxob = pc._catalog.getIndex(idx)
    fh = open('%s/%s.dump' % (dumppath, idx), 'wb')
    cPickle.dump(aq_inner(aq_base(idxob)), fh)
    fh.close()
    msg += "dumped %s\n" % idx
  return msg

def restore(self):
  pc = self.portal_catalog
  msg = ""
  for idx in IDX:
    fh = open('%s/%s.dump' % (dumppath, idx), 'rb')
    idxob = cPickle.load(fh)
    pc._catalog.addIndex(idx,idxob)
    fh.close()
    msg += "restored %s\n" % idx
  return msg

        
