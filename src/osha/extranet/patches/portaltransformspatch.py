import logging
from Products.PortalTransforms.unsafe_transforms.command import ExternalCommandTransform, popen3

log = logging.getLogger('osha.extranet')
log.info('PATCHING Products.PortalTransforms')

def _invokeCommand(self, input_name):
    command = '%(binary_path)s %(command_line)s' % self.config
    params = dict(input=input_name)
    input, output, error = popen3(command % params)
    input.close()
    # first read stderr, else we may hang on stout
    error_data = error.read()
    error.close()
    data = output.read()
    output.close()
    if error_data and not data:
        data = error_data
    elif error_data and not data:
        log.error('Error while running "%s":\n %s' % (command % params,
                                                error_data))
    return data

ExternalCommandTransform.invokeCommand = _invokeCommand
