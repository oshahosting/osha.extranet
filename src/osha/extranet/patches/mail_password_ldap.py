from Products.CMFCore.utils import getToolByName
from Products.CMFPlone import RegistrationTool
from Products.PasswordResetTool import PasswordResetTool
from zope.i18nmessageid import MessageFactory

import logging
log = logging.getLogger(__name__)

_ = MessageFactory('plone')
log.info('Patching CMFPlone.RegistrationTool:get_member_by_login_name')


def _get_member_by_login_name(context, login_name, raise_exceptions=True):
    """ Override to get the userid from an email address
    """
    membership = getToolByName(context, 'portal_membership')
    # First the easy case: it may be a userid after all.
    member = membership.getMemberById(login_name)
    if member is not None:
        return member

    # Try to find this user via the login name.
    acl = getToolByName(context, 'acl_users')
    userids = [user.get('userid') for user in
               acl.searchUsers(email=login_name, exact_match=True)
               if user.get('userid')]
    if userids:
        userid = userids[0]
        member = membership.getMemberById(userid)
    if len(userids) > 1:
        log.error(
            'Multiple users ({}) found for this email address: {}'.format(
                ', '.join(userids), login_name
            )
        )
    if member is None and raise_exceptions:
        raise ValueError(_(u'The username you entered could not be found: {}'.format(login_name)))
    return member

PasswordResetTool.get_member_by_login_name = _get_member_by_login_name
RegistrationTool.get_member_by_login_name = _get_member_by_login_name
