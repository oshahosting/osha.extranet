# This makes a ploneflare a bit more lenient. I contacted witsch for a better solution 
# This should be removed if the flares are properly initialized with all attributes

from logging import getLogger

log = getLogger()

# fcuk

from collective.solr.flare import PloneFlare
from collective.solr import indexer

def getRID(self):
    """ Return a Resource Identifier, like a brain would do """
    return self.UID

PloneFlare.getRID = getRID
PloneFlare._unrestrictedGetObject = PloneFlare.getObject

def inthandler(value):
    if value is None:
        raise AttributeError
    return value

for field in ['solr.IntField', 'solr.TrieIntField']:
    if indexer.handlers.has_key(field):
        log.info("Please review the solr patches")
    else:
        indexer.handlers[field] = inthandler
