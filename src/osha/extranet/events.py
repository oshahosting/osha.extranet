from Products.CMFPlone.i18nl10n import utranslate
from Products.CMFCore.utils import getToolByName

def blog_added(self, event):
    """ Add a collection inside the Blog
    """
    blog = event.object
    if blog.portal_type != 'Blog':
        return

    pt = getToolByName(blog, 'portal_types')
    info = pt.getTypeInfo('Topic')
    topic = info._constructInstance(blog, 'Topic')
    topic.setTitle(utranslate(
                        'blog', 
                        u'Latest blog entries', 
                        context=blog))
    topic.reindexObject()

    crit = topic.addCriterion(
                            'Type',
                            'ATPortalTypeCriterion')
    crit.setValue('Blog Entry')
    
    crit = topic.addCriterion(
                            'effective',
                            'ATSortCriterion')

    crit = topic.addCriterion(
                            'path',
                            'ATPathCriterion')
    crit.setValue(blog.UID())
    crit.setRecurse(True)

    topic.getSortCriterion().setReversed(True)

    blog.setDefaultPage(topic.getId())


