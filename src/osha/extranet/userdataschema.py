from zope.interface import implements
from zope import schema
from zope.schema.vocabulary import SimpleVocabulary
from plone.app.users.userdataschema import IUserDataSchemaProvider, IUserDataSchema
from zope.i18nmessageid import MessageFactory
_ = MessageFactory("osha.extranet")

class UserDataSchemaProvider(object):
    implements(IUserDataSchemaProvider)

    def getSchema(self):
        """
        """
        return IExtranetUserDataSchema

class IExtranetUserDataSchema(IUserDataSchema):
    """ Use all the fields from the default user data schema, and add various
    extra fields.

    The order is relevant for the registration form and the personal
    information form. These forms have the same order, but registration form
    skips some fields.

    XXX: description (inherited from IUserDataSchema was between 'sn' and
    'ictGender'. If we want that, we need to declare it here again or patch
    plone.app.users to support a tuple for order of fields.

    XXX: ditto, location was between Birthday and TimeZone, home_page between
    department and alternativeEmail
    """
    pers_title = schema.TextLine(
        title=_(u'label_pers_title', default=u'Title'),
        description=_(u'help_pers_title',
                      default=u""),
        required=False,
        )

    givenName = schema.TextLine(
        title=_(u'label_givenName', default=u'First Name'),
        description=_(u'help_givenName',
                      default=u""),
        required=False,
        )

    initials = schema.TextLine(
        title=_(u'label_initials', default=u'Middle Initial'),
        description=_(u'help_initials',
                      default=u""),
        required=False,
        )

    sn = schema.TextLine(
        title=_(u'label_sn', default=u'Last Name'),
        description=_(u'help_sn',
                      default=u""),
        required=False,
        )

    ictGender = schema.Choice(
        title=_(u'label_ictGender', default=u'Gender'),
        description=_(u'help_ictGender',
                      default=u""),
        vocabulary = SimpleVocabulary.fromItems([('male', 'Male'), ('female', 'Female')]),
        required=False,
        )

    ictBirthday = schema.TextLine(
        title=_(u'label_ictBirthday', default=u'Birthday'),
        description=_(u'help_ictBirthday',
                      default=u""),
        required=False,
        )

    ictTimeZone = schema.TextLine(
        title=_(u'label_ictTimeZone', default=u'Time Zone'),
        description=_(u'help_ictTimeZone',
                      default=u""),
        required=False,
        )

    department= schema.TextLine(
        title=_(u'label_department', default=u'Department'),
        description=_(u'help_department',
                      default=u""),
        required=False,
        )

    alternativeEmail = schema.TextLine(
        title=_(u'label_alternativeEmail', default=u'Alternative E-mail'),
        description=_(u'help_alternativeEmail',
                      default=u""),
        required=False,
        )

    telephoneNumber = schema.TextLine(
        title=_(u'label_telephoneNumber', default=u'Phone'),
        description=_(u'help_telephoneNumber',
                      default=u""),
        required=False,
        )

    mobileTelephoneNumber = schema.TextLine(
        title=_(u'label_mobileTelephoneNumber', default=u'Cellular Phone'),
        description=_(u'help_mobileTelephoneNumber',
                      default=u""),
        required=False,
        )

    facsimileTelephoneNumber = schema.TextLine(
        title=_(u'label_facsimileTelephoneNumber', default=u'Fax'),
        description=_(u'help_facsimileTelephoneNumber',
                      default=u""),
        required=False,
        default=u'',
        )

    pager = schema.TextLine(
        title=_(u'label_pager', default=u'Pager'),
        description=_(u'help_pager',
                      default=u""),
        required=False,
        )

    homePostalAddress = schema.TextLine(
        title=_(u'label_homePostalAddress', default=u'Home Address'),
        description=_(u'help_homePostalAddress',
                      default=u""),
        required=False,
        )

    homeTelephoneNumber = schema.TextLine(
        title=_(u'label_homeTelephoneNumber', default=u'Home Phone'),
        description=_(u'help_homeTelephoneNumber',
                      default=u""),
        required=False,
        )

    ictHomeFacsimileTelephoneNumber = schema.TextLine(
        title=_(u'label_ictHomeFacsimileTelephoneNumber', default=u'Home Fax'),
        description=_(u'help_ictHomeFacsimileTelephoneNumber',
                      default=u""),
        required=False,
        )

    ictFavoriteLinks = schema.List(
        title=_(u'label_ictFavoriteLinks', default=u'My Favorite Links'),
        description=_(u'help_ictFavoriteLinks',
                      default=u""),
        required=False,
        value_type = schema.TextLine(
            title = _(u'label_ictFavoriteLinksValue', default=u'Link'),
            ),
        )

    oshPersSubjects = schema.List(
        title=_(u'label_oshPersSubjects', default=u'Interests'),
        description=_(u'help_oshPersSubjects',
                      default=u""),
        required=False,
        value_type = schema.TextLine(
            title = _(u'label_ictFavoriteLinksValue', default=u'Link'),
            ),
        )
