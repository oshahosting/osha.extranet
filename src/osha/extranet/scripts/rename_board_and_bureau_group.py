from Acquisition import aq_inner, aq_parent, aq_base, aq_chain, aq_get
from Products.CMFCore.utils import getToolByName
membership_tool = getToolByName(app.extranet, 'portal_membership')
from AccessControl.SecurityManagement import newSecurityManager, getSecurityManager
user = app.acl_users.getUser('admin').__of__(app.extranet.acl_users)
newSecurityManager(None, user)
getSecurityManager().getUser()
extranet = app.extranet
pc = extranet.portal_catalog
pm = extranet.portal_membership
import transaction

def main(self):
    # raise Exception("This script must be customised before running")
    portal = self.portal_url.getPortalObject()

    def log(message):
        print("\n"+message)
        # self.REQUEST.response.write("\n"+str(message))

    pc = self.portal_catalog
    current_path = "/".join(self.getPhysicalPath())
    sub_ob_brains = pc.searchResults({"path":current_path},
    limit=999999999)
    # the limit seems to do nothing
    count = 20
    import pdb; pdb.set_trace()
    for brain in sub_ob_brains:
        log("*")
        try:
            if brain:
                ob = brain.getObject()
                bnb_roles = ob.get_local_roles_for_userid("Board & Bureau")
                if bnb_roles:
                    ob.manage_addLocalRoles("BoardAndBureau", bnb_roles)
                    ob.manage_delLocalRoles(("Board & Bureau",))
                    ob.reindexObject()
                    log("Roles for %s are now: %s" %(
                        ob.absolute_url(), ob.get_local_roles()
                        )
                        )
                if count == 0:
                    transaction.commit()
                    count = 20
                else:
                    count -= 1
            transaction.commit()
        except:
            transaction.commit()
            import pdb; pdb.set_trace()
    log("done")
 
main(extranet)
