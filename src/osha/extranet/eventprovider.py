from p4a.plonecalendar.eventprovider import BrainEvent, RecurringBrainEvent
from Products.CMFCore.utils import getToolByName
import datetime
from zope import interface
from zope import component
from dateable import kalends
from collective.solr.parser import SolrResponse
from collective.solr.flare import PloneFlare

class SolrBrainEvent(BrainEvent):
    interface.implements(kalends.ITimezonedOccurrence)
    component.adapts(PloneFlare)

    def __init__(self, context, date=None):
        self.context = context
        self.event = None
        putils = getToolByName(self.context.getObject(), 'plone_utils')
        self.encoding = putils.getSiteEncoding()
        self.date = date


class RecurringSolrBrainEvent(SolrBrainEvent):

    interface.implements(kalends.ITimezonedRecurringEvent)
    component.adapts(PloneFlare)
    
    def getOccurrences(self, start, stop):
        if start is not None:
            startdate = start.toordinal()
        if stop is not None:
            stopdate = stop.toordinal()
        event = self._getEvent()
        try:
            recurrence = kalends.IRecurrence(event)
        except TypeError:
            # Could not adapt to IRecurrence, which means no recurrence
            # support is installed.
            return []

        res = []

        for each in recurrence.getOccurrenceDays():
            if start is not None and each < startdate:
                continue
            if stop is not None and each > stopdate:
                break
            dt = datetime.date.fromordinal(each)
            res.append(BrainEvent(self.context, dt))

        return res
