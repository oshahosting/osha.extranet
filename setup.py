from setuptools import setup
import os

version = '2.1.dev0'

tests_require=[
            'zope.testing',
            ]

setup(
    name='osha.extranet',
    version=version,
    description="This is the policy product for the OSHA extranet",
    long_description=open("README.txt").read() + "\n" +
                    open(os.path.join("docs", "HISTORY.txt")).read(),
    # Get more strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        "Framework :: Plone",
        "Programming Language :: Python",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "License :: OSI Approved :: GNU General Public License (GPL)",
        "License :: OSI Approved :: European Union Public Licence 1.1 (EUPL 1.1)",
        ],
    keywords='plone syslab.com osha extranet',
    author='Syslab.com GmbH',
    author_email='info@syslab.com',
    url='http://svn.plone.org/svn/plone/plone.example',
    license='GPL',
    packages=['osha', 'osha/extranet'],
    package_dir = {'' : 'src'},
    namespace_packages=['osha'],
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'Products.ATVocabularyManager',
        'Products.CMFEditions',
        'Products.CMFNotification',
        'Products.LinguaPlone',
        'Products.MaildropHost',
        'python-memcached',
        'Products.LoginLockout',
        'Products.MemcachedManager',
        'Products.PloneFlashUpload',
        'Products.PloneHelpCenter',
        'Products.PlonePopoll',
        'Products.Ploneboard',
        'Products.Scrawl',
        'Products.SecureMaildropHost',
        'Products.TextIndexNG3',
        'Products.VocabularyPickerWidget',
        'actionbar.babble',
        'archetypes.schemaextender',
        'babble.server',
        'collective.js.treetable',
        'collective.plonetruegallery',
        'collective.solr',
        'collective.xdv',
        'collective.zipfiletransport',
        'ictextranet.contenttypes',
        'ictextranet.theme',
        'p4a.plonecalendar',
        'p4a.subtyper',
        'plone.app.caching',
        'plone.app.relations',
#        'plone.contentratings',
        'plone.reload',
        'setuptools',
        'slc.autocategorize',
        'slc.clicksearch',
        'slc.fileshare',
        'slc.statisticsannotation',
#        'slc.stickystatusmessages',
        'slc.tasks',
        'slc.teamfolder',
        'zopyx.smartprintng.plone',
    ],
    tests_require=tests_require,
    extras_require=dict(tests=tests_require),
    entry_points="""
    [z3c.autoinclude.plugin]
    target = plone
    """,
    setup_requires=["PasteScript"],
    paster_plugins = ["ZopeSkel"],
    )
